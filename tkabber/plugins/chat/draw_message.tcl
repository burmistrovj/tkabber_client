# $Id$

proc draw_message {chatid user body type} {
    variable F
    if {![cequal $type groupchat] && \
	    [hook::is_flag chat_send_message_hook draw]} {
	chat::add_message $chatid [chat::our_jid $chatid] $type $body {}
    }
    hook::unset_flag chat_send_message_hook draw
}
hook::add chat_send_message_hook [namespace current]::draw_message 91
