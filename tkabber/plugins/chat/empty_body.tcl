# $Id$

proc check_send_empty_body {chatid user body type} {
    if {[cequal $body ""]} {
	return stop
    }
}
hook::add chat_send_message_hook [namespace current]::check_send_empty_body 10

proc check_draw_empty_body {chatid from type body x} {
    if {[cequal $body ""]} {
	return stop
    }
}
hook::add draw_message_hook [namespace current]::check_draw_empty_body 4

