$Id$

"Quote last message" -- chat plugin for Tkabber.


I. The idea

This plugin provides a way to "quote" the message located at the
very bottom of the conversation log of a chat window -- that is,
to copy such message into the input box of the same chat window
with some decorations added (by default the ">> " string is
prepeneded to the message). By default, quoting is done using
the <Alt-q> keyboard shortcut.

The functionality provided by this plugin might come in handy
when you want to refer to someone else's (or your own) message
appeared in the course of the conversation.


II. Configuration

You can configure two parameters affecting the operation of this
plugin: actions triggering the quotation of the last message
and the format string used to decorate the message being quoted.

Message quoting action is bound to a Tk virtual event named
<<QuoteLastMessage>>. By default, it's bound to <Alt-q> (also to
<Meta-q> to accound for certain Unix systems).
To change the binding you will have to add appropriate
command(s) to the Tkabber configuration file. For instance, to
unbind this action from <Alt-q> and bind it to <Ctrl-Shift-m>
write this into your condig.tcl:

event delete <<QuoteLastMessage>> <Alt-q>
event delete <<QuoteLastMessage>> <Meta-q>
event add    <<QuoteLastMessage>> <Control-Shift-m>

Full details about the "event" command can be found in [1].
Note that you can use any event specifier, not just keyboard
shortcuts. Refer to [2] for full details.

The format string used to decorate the message being quoted can
be changed using the Customize interface in the "Plugins ->
Quote Last Message" group of settings (also reachable as
"Chat -> Quote Last Message").
The actual setting is named
::plugins::quotelastmsg::options(format)
and it defaults to the string ">> %m".

In the format string the format specifier "%m" is substituted by
the whole message as seen in the conversation log window, and
the specifier "%%" is replaced by a single character "%".


III. Bugs and limitations

* There's currently no way to separately refer to the parts of
  the message being quoted (such as its timestamp, poster's
  nickname and the message body) in the format string.

* It's impossible to embed "special chanracters" into the format
  string (such as \n or \t).


IV. References

1. http://www.tcl.tk/man/tcl8.5/TkCmd/event.htm
2. http://www.tcl.tk/man/tcl8.5/TkCmd/bind.htm


vim:et:ts=4:sw=4:tw=64
