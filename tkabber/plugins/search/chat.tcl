# $Id$

namespace eval search {}

namespace eval search::chat {
    hook::add open_chat_post_hook [namespace current]::setup_panel
}

proc search::chat::open_panel {chatw sf} {
    pack $sf -side bottom -anchor w -fill x -before [winfo parent $chatw].csw
    update idletasks
    $chatw see end
}

proc search::chat::close_panel {chatid sf} {
    set cw [chat::winid $chatid]
    set chatw [chat::chat_win $chatid]

    $chatw tag remove search_highlight 0.0 end
    pack forget $sf
    focus $cw.input
}

proc search::chat::setup_panel {chatid type} {
    set cw [chat::winid $chatid]
    set chatw [chat::chat_win $chatid]

    $chatw mark set sel_start end
    $chatw mark set sel_end 0.0

    set sf [plugins::search::spanel [winfo parent $chatw].search \
		-searchcommand [list [namespace parent]::do_text_search $chatw] \
		-closecommand [list [namespace current]::close_panel $chatid]]

    bind $cw.input <<OpenSearchPanel>> \
	[double% [list [namespace current]::open_panel $chatw $sf]]
}

