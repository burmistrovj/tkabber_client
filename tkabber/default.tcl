# $Id$

proc postload {} {}

proc menuload {menudesc} { return $menudesc }

proc finload  {} {}

proc browser_loadopt {} {
    custom::defvar webbrowser "" \
	[::msgcat::mc "Command to be run when you click a URL\
		       in a message. '%s' will be replaced with this\
		       URL (e.g. \"galeon -n %s\")."]\
	-group IFace -type string
}

hook::add postload_hook browser_loadopt

proc browseurl {url} {
    global env tcl_platform

    set_status $url
    update idletask

    set url [string map {\u0009 %09 \u000a %0a \u000d %0d \u0020 %20
			 \" %22 \' %27 \< %3c \> %3e
			 \{ %7b \| %7c \} %7d \\ %5c} $url]

    debugmsg browseurl "Mapped URL: '$url'"

    if {[info exists ::webbrowser] && $::webbrowser != ""} {
	# If user specified a browser, use it

	set cmd {exec}
	if {[catch { llength $::webbrowser }]} {
	    error "Can't process the \$::webbrowser variable. It must be a list of words."
	} else {
	    foreach word $::webbrowser {
		lappend cmd [format $word $url]
	    }
	}
	lappend cmd &
	eval $cmd
	return
    }

    # Set the clipboard value to this url in-case the user needs to paste the 
    # url in (some windows systems).

    clipboard clear
    clipboard append $url

    switch -- $tcl_platform(platform) {
        windows {
	    # The windows NT shell treats '&' as a special character. Using
	    # a '^' will escape it. See http://wiki.tcl.tk/557 for more info. 

            if {[string compare $tcl_platform(os) "Windows NT"] == 0} { 
                set url [string map {& ^&} $url]
            }
            if {[catch {eval exec [auto_execok start] [list $url] &} emsg]} {
                MessageDlg .browse_err -aspect 50000 -icon info \
		    -buttons ok -default 0 -cancel 0 -type user \
                    -message \
			[::msgcat::mc "Error displaying %s in browser\n\n%s" \
				      $url $emsg]
            }
        }
        macintosh {
            if {[info exists env(BROWSER)]} {
		set browser $env(BROWSER)
	    } else {
                set browser "Browse the Internet"
	    }
            AppleScript execute \
                "tell application \"$browser\"\nopen url \"$url\"\nend tell\n"
        }
        default {
            if {[info exists env(BROWSER)]} {
		set browser $env(BROWSER)
	    } else {
                foreach b [list xdg-open sensible-browser \
				chrome chromium \
				firefox mozilla-firefox iceweasel \
				mozilla netscape \
                                opera konqeror] {
                    if {[llength [set e [auto_execok $b]]] > 0} {
                        set browser [lindex $e 0]
                        break
                    }
                }
	    }
            if {![info exists browser]} {
		if {[string equal $tcl_platform(os) Darwin]} {
		    exec open $url &
		} else {
		    MessageDlg .browse_err -aspect 50000 -icon info \
                        -message [::msgcat::mc "Please define environment variable BROWSER"] \
			-type user \
			-buttons ok -default 0 -cancel 0
		}
            } else {
		debugmsg browseurl "Browser: '$browser'"
		exec $browser $url &
	    }
	}
    }

    set_status ""
}
