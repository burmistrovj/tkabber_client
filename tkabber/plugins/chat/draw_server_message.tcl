# $Id$

proc handle_server_message {chatid from type body x} {
    set jid [chat::get_jid $chatid]
    if {([cequal $type groupchat] && [string equal $jid $from]) \
	|| ([cequal $type chat] && $from == "")} {
	set chatw [chat::chat_win $chatid]

	$chatw insert end --- server_lab " "
	::richtext::render_message $chatw $body server
	return stop
    }
}
hook::add draw_message_hook [namespace current]::handle_server_message 20

# vim:ts=8:sw=4:sts=4:noet
