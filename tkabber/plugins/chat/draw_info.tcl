# $Id$

proc handle_info {chatid from type body x} {

    if {[cequal $type info]} {
	::richtext::render_message [chat::chat_win $chatid] $body info
	return stop
    }
}
hook::add draw_message_hook [namespace current]::handle_info 10

