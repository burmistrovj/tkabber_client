# $Id$

proc handle_error {chatid from type body x} {

    if {[cequal $type error]} {
	set chatw [chat::chat_win $chatid]
	$chatw insert end $body err

	set cw [chat::winid $chatid]

	return stop
    }
}
hook::add draw_message_hook [namespace current]::handle_error 10
