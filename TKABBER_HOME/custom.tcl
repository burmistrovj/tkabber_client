{::::gr_server_list {conference.jabber.ru conference.shipyard-yantar.ru}}
{::plugins::loaded(receipts) 1}
{::ifacetk::options(font) {Arial 12}}
{::plugins::stylecodes::options(hide_markup) 0}
{::plugins::nickcolors::options(use_colored_messages) 1}
{::sound::options(presence_unavailable_sound) {}}
{::disco::browser::disco_list {jud.shipyard-yantar.ru shipyard-yantar.ru jabber:iq:search broadcast.shipyard-yantar.ru jabber.ru conference.jabber.ru}}
{::ifacetk::roster::show_only_online 1}
{::plugins::loaded(floatinglog) 1}
{::muc::options(history_maxchars) 90000}
{::plugins::nickcolors::options(use_colored_roster_nicks) 1}
{::ifacetk::options(closebuttonaction) minimize}
{::plugins::loaded(attline) 1}
{::ifacetk::roster::options(match_jids) 1}
{::plugins::emoticons::options(theme) D:/Tkabber/Tkabber/emoticons/default}
{::ifacetk::systray::options(blink_type) 2}
{::plugins::loaded(socials) 1}
{::pixmaps::options(pixmaps_theme) {Default Blue}}
{::plugins::loaded(quotelastmsg) 1}
{::ifacetk::roster::options(chats_group) 1}
{::ifacetk::roster::options(show_conference_user_info) 1}
{::plugins::loaded(jidlink) 1}
{::ifacetk::roster::options(use_filter) 1}
{::plugins::geometry::MainWindowGeometry {:0.0 788x550+52+52}}
{::plugins::loaded(floatingcontact) 1}
{::plugins::loaded(whiteboard) 1}
{::muc::options(history_maxstanzas) 3000}
{::ifacetk::roster::options(show_own_resources) 1}
{::::gr_group_list {tkabber ы confa4}}
{::plugins::geometry::MainWindowState {:0.0 zoomed}}
{::::userpriority 8}
{::plugins::loaded(cyrillize) 1}
{::plugins::cache_categories::category_and_subtype_list {shipyard-yantar.ru {server im} conference.shipyard-yantar.ru {conference text} {} {server im} jabber.ru {pubsub pep} conference.jabber.ru {conference text}}}
{::plugins::loaded(singularity) 1}
{::::gr_nick_list {burmistrovj e.burmistrov}}
{::muc::options(gen_muc_position_change_msgs) 1}
{::sound::options(presence_available_sound) {}}
{::disco::browser::node_list {{} http://jabber.org/protocol/commands}}
{::plugins::loaded(stripes) 1}
{::plugins::chatinfo::options(desc) 1}
{::plugins::clientinfo::options(autoask) 1}
{::plugins::loaded(spy) 1}
{::plugins::geometry::MainWindowGeometryUntabbed {:0.0 950x700-70+100}}
{::plugins::options(timestamp_format) {[%a %T]}}
{::ifacetk::status_history {{Автоматически в состоянии "отошёл" по бездействию}}}
{::ifacetk::roster::options(show_subscription) 1}
{::plugins::log_on_open::options(max_interval) -1}
{::plugins::nickcolors::options(use_colored_nicks) 1}
{::plugins::khim::settings {khim::setConfig 1.0 1 Pause {
    !! \u00a1
    {"A} \u00c4
    {"a} \u00e4
    {"E} \u00cb
    {"e} \u00eb
    {"I} \u00cf
    {"i} \u00ef
    {"O} \u00d6
    {"o} \u00f6
    {"U} \u00dc
    {"u} \u00fc
    'A \u00c1
    'a \u00e1
    'E \u00c9
    'e \u00e9
    'I \u00cd
    'i \u00ed
    'O \u00d3
    'o \u00f3
    'U \u00da
    'u \u00fa
    'Y \u00dd
    'y \u00fd
    *A \u00c5
    *a \u00e5
    ,C \u00c7
    ,c \u00e7
    -> \u2192
    -L \u0141
    -l \u0142
    /O \u00d8
    /o \u00f8
    12 \u00bd
    13 \u2153
    14 \u00bc
    18 \u215b
    23 \u2154
    34 \u00be
    38 \u215c
    58 \u215d
    78 \u215e
    :( \u2639
    :) \u263a
    <- \u2190
    << \u00ab
    >> \u00bb
    ?? \u00bf
    ^A \u00c2
    ^a \u00e2
    ^E \u00ca
    ^e \u00ea
    ^I \u00ce
    ^i \u00ee
    ^O \u00d4
    ^o \u00f4
    ^U \u00db
    ^u \u00fb
    `A \u00c0
    `a \u00e0
    `E \u00c8
    `e \u00e8
    `I \u00cc
    `i \u00ec
    `O \u00d2
    `o \u00f2
    `U \u00d9
    `u \u00f9
    AA \u00c5
    aa \u00e5
    AE \u00c6
    ae \u00e6
    bu \u2022
    de \u00b0
    eu \u20ac
    LP \u2615
    mu \u00b5
    OC \u00a9
    OE \u0152
    oe \u0153
    OR \u00ae
    ss \u00df
    |c \u00a2
    ~A \u00c3
    ~a \u00e3
    ~N \u00d1
    ~n \u00f1
    ~O \u00d5
    ~o \u00f5
}}}
{::muc::options(gen_muc_status_change_msgs) 1}
{::plugins::options(delayed_timestamp_format) {[%a %Y-%m-%d %R]}}
{::plugins::loaded(aniemoticons) 1}
