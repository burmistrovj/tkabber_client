# $Id$

custom::defvar options(reply_iq_last) 1 \
    [::msgcat::mc "Reply to idle time (jabber:iq:last) requests."] \
    -group IQ -type boolean

proc iq_last {xlib from child args} {
    global idle_command
    global userstatus statusdesc textstatus
    variable options

    if {$options(reply_iq_last) && [info exists idle_command]} {
	set seconds [expr {[eval $idle_command]/1000}]
	set status $statusdesc($userstatus)
	if {[cequal $textstatus ""]} {
	    set status "$statusdesc($userstatus) ($userstatus)"
	} else {
	    set status "$textstatus ($userstatus)"
	}
	return [list result [::xmpp::xml::create query \
				    -xmlns jabber:iq:last \
				    -attrs [list seconds $seconds] \
				    -cdata $status]]
    } else {
	return [list error cancel service-unavailable]
    }
}

::xmpp::iq::register get query jabber:iq:last [namespace current]::iq_last

