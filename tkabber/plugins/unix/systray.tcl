# $Id$

# Freedesktop systray icon support.
# Requires Tray package
# (ftp://ftp.atmsk.ru/pub/tkabber/tksystray.tar.gz)

##########################################################################

if {![cequal $::interface tk]} return

if {[catch { package require Tray }]} return

##########################################################################

namespace eval systray {
    variable options

    custom::defvar options(enable) 1 \
	[::msgcat::mc "Enable freedesktop systray icon."] \
	-group Systray -type boolean \
	-command [namespace code enable_disable]
}

##########################################################################

proc systray::set_current_theme {} {
    variable s2p
    foreach {k v} [list available   available      \
                        away        away           \
                        chat        chat           \
                        dnd         dnd            \
                        xa          xa             \
                        unavailable unavailable    \
                        invisible   invisible      \
			blank       blank          \
			message1    message-server \
			message2    message        \
			message3    message-personal] {
        set s2p($k) [pixmaps::get_filename docking/$v]
    }
}

hook::add set_theme_hook [namespace current]::systray::set_current_theme

##########################################################################

proc systray::enable_disable {args} {
    variable options

    set icon .si

    if {$options(enable) && ![winfo exists $icon]} {
	ifacetk::systray::create $icon \
	    -createcommand [namespace code create] \
	    -configurecommand [namespace code configure] \
	    -destroycommand [namespace code destroy]
    } elseif {!$options(enable) && [winfo exists $icon]} {
	ifacetk::systray::destroy $icon
    }
}

hook::add finload_hook [namespace current]::systray::enable_disable

##########################################################################

proc systray::create {icon} {
    variable s2p

    newti $icon -pixmap $s2p(unavailable)

    set m [ifacetk::systray::popupmenu $icon.menu]

    bind $icon <ButtonRelease-1> ifacetk::systray::restore
    bind $icon <<PasteSelection>> ifacetk::systray::withdraw
    bind $icon <<ContextMenu>> [list tk_popup [double% $m] %X %Y]
    balloon::setup $icon -command [list ifacetk::systray::balloon $icon]
}

##########################################################################

proc systray::configure {icon status} {
    variable s2p

    if {![cequal $icon ""] && [winfo exists $icon]} {
	configureti $icon -pixmap $s2p($status)
    }
}

##########################################################################

proc systray::destroy {icon} {
    if {![cequal $icon ""] && [winfo exists $icon]} {
	removeti $icon
	::destroy $icon
    }
}

##########################################################################

