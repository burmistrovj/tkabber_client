;#TKABBER SHIPYARD-YANTAR.RU CONFIG
option readfile $rootdir/examples/xrdb/teopetuk.xrdb userDefault
option add *Chat.oddBackground  gray93;option add *Chat.evenBackground  gray91
option add *Tkabber.mainRosterWidth 250 userDefault;option add *Tkabber.chatRosterWidth 250 userDefault;option add *Chat.inputheight 10 userDefault;option add *Scrollbar.width 8 userDefault
option add *mainRosterWidth 250;option add *font -monotype-arial-medium-r-*-*-13-*-100-100-*-*-iso10646-1;set font -monotype-arial-medium-r-*-*-13-*-100-100-*-*-iso10646-1
;#Check for OS variables by OS type and variable values
if {($::tcl_platform(platform) == "windows")} {set TkHome $env(TKABBER_HOME)
	if { [info exists ::env(USERDNSDOMAIN)] } {set LocConfProfile "YantarUser";set LogConfServer $env(USERDNSDOMAIN);set LogConfProxyhost proxy.$env(USERDNSDOMAIN);set LocConfServerAlt jabber.$env(USERDNSDOMAIN)} \
		else {set LogConfServer "";set LogConfProxyhost "";set LocConfServerAlt ""}
	if { [info exists ::env(USERNAME)] } {set LogConfUser $env(USERNAME)} else {set LogConfUser ""}
	if { [info exists ::env(COMPUTERNAME)] } {set LogConfResource $env(COMPUTERNAME)} else {set LogConfResource ""}
} else {}
if {$::tcl_platform(platform) == "unix"} {set TkHome $env(TKABBER_HOME)
	if { [info exists ::env(USERDNSDOMAIN)] } {set LocConfProfile "YantarUser";set LogConfServer $env(USERDNSDOMAIN);set LogConfProxyhost proxy.$env(USERDNSDOMAIN);set LocConfServerAlt jabber.$env(USERDNSDOMAIN)} \
		else {set LogConfServer "";set LogConfProxyhost "";set LocConfServerAlt ""}
	if { [info exists ::$env(USERNAME)] } {set LogConfUser $env(USERNAME)} else {set LogConfUser ""}
	if { [info exists ::$env(HOSTNAME)] } {set LogConfResource $env(HOSTNAME)} else {set LogConfResource ""}
} else {}
if {($::tcl_platform(platform) != "windows")&&($::tcl_platform(platform) != "unix")} {set LogConfUser "";set LogConfResource ""} else {}
if {($LogConfServer != "SHIPYARD-YANTAR.RU")} {set LocConfProfile "DefaultUser";set LogConfUser "";set LogConfResource "";set LogConfServer "";set LocConfServerAlt "";set LogConfProxyhost ""} else {}
;#
set loginconf1(profile)			"$LocConfProfile"
set loginconf1(user)			"$LogConfUser"
set loginconf1(resource)		"$LogConfResource"
set loginconf1(server)			"$LogConfServer"
set loginconf1(usedigest)		"0"
set loginconf1(usealtserver)	"1"
set loginconf1(altserver)		"$LocConfServerAlt"
set loginconf1(altport)			"5222"
;#set loginconf1(stream_options)	"plaintext"
set loginconf1(allowauthplain)	"1"
set loginconf1(useproxy)		"0"
set loginconf1(stream_options)	"encrypted"
set loginconf1(sslcacertstore)	"$TkHome/c01.pem"
set loginconf1(httpproxy)		"$LogConfProxyhost"
set loginconf1(httpproxyport)	"3128"
set loginconf1(proxyusername)	"$LogConfUser"
set loginconf1(proxypassword)	""
;#
set ifacetk::options(use_tabbar) 1
set show_splash_window 0
set debug_lvls [list message presence ssj warning xmpp]
array set loginconf [array get loginconf1]
set loginconf(replace_opened) 0
set autologin 0
proc postload {} {
set ::plugins::floatinglog::options(show_log) 1
}
;#ADDED FUNCTIONALITY:
hook::add finload_hook {
	bind . <Control-Key-F1>		{set userstatus available}
	bind . <Control-Key-F2>		{set userstatus away}
	bind . <Control-Left>		{ifacetk::tab_move .nb -1}
	bind . <Control-Right>		{ifacetk::tab_move .nb  1}
	bind . <Alt-Left>			{ifacetk::current_tab_move .nb -1}
	bind . <Alt-Right>			{ifacetk::current_tab_move .nb  1}
	;#bind . <Shift-Escape>		{::ifacetk::systray::withdraw}		;#EXPLAIN: withdraw tkabber to system tray
	bind . <Alt-Key-F1>			{ifacetk::quick_help_window}		;#EXPLAIN: show quick help message
	bind . <Alt-Key-F4>			{quit}								;#EXPLAIN: exit from tkabber
	bind . <Alt-`>				{console show}						;#EXPLAIN: show console
	;#Change search panel command
	event add <<OpenSearchPanel>> <Control-f>		;#Disabled original at /plugins/search.tcl
	event add <<OpenSearchPanel>> <Control-agrave>	;#Disabled original at /ifacetk/iface.tcl	;#Russian "CTRL+f" key combination
	event add <<OpenSearchPanel>> <Control-F>		;#
	event add <<OpenSearchPanel>> <Control-Agrave>	;#Russian "CTRL+F" key combination
	;#EXPLAIN: Close current chat tab by pressing "Ctrl+w"
	bind . <Control-w> { if {[.nb raise] != ""} {eval destroy [pack slaves [.nb getframe [.nb raise]]]		;.nb delete [.nb raise] 1;		ifacetk::tab_move .nb 0		;};		}
	bind . <Control-W> { if {[.nb raise] != ""} {eval destroy [pack slaves [.nb getframe [.nb raise]]]		;.nb delete [.nb raise] 1;		ifacetk::tab_move .nb 0		;};		}
	bind . <Control-odiaeresis> { if {[.nb raise] != ""} {eval destroy [pack slaves [.nb getframe [.nb raise]]]		;.nb delete [.nb raise] 1;		ifacetk::tab_move .nb 0		;};		}
	bind . <Control-Odiaeresis> { if {[.nb raise] != ""} {eval destroy [pack slaves [.nb getframe [.nb raise]]]		;.nb delete [.nb raise] 1;		ifacetk::tab_move .nb 0		;};		}
	;#EXPLAIN: Show current user history by pressing "CTRL+h"
	proc get_user_history1 {chatid type} {bind [chat::input_win $chatid] <Control-h> \;[list ::logger::show_log [::xmpp::jid::stripResource [chat::get_jid $chatid]] -connection [chat::get_xlib $chatid]];}
	proc get_user_history2 {chatid type} {bind [chat::input_win $chatid] <Control-H> \;[list ::logger::show_log [::xmpp::jid::stripResource [chat::get_jid $chatid]] -connection [chat::get_xlib $chatid]];}
	proc get_user_history3 {chatid type} {bind [chat::input_win $chatid] <Control-eth> \;[list ::logger::show_log [::xmpp::jid::stripResource [chat::get_jid $chatid]] -connection [chat::get_xlib $chatid]];}
	proc get_user_history4 {chatid type} {bind [chat::input_win $chatid] <Control-Eth> \;[list ::logger::show_log [::xmpp::jid::stripResource [chat::get_jid $chatid]] -connection [chat::get_xlib $chatid]];}
	hook::add open_chat_post_hook get_user_history1;hook::add open_chat_post_hook get_user_history2;hook::add open_chat_post_hook get_user_history3;hook::add open_chat_post_hook get_user_history4
}