# $Id$
# Titlebar icons support. Works with Tk 8.5 or newer.

##########################################################################

namespace eval icon {
    hook::add finload_hook [namespace current]::win_icons
}

##########################################################################

proc icon::win_icons {} {
    # Do not load static icon if a WindowMaker dock is used.
    if {[info exists ::wmaker_dock] && $::wmaker_dock} {
	return
    }

    if {[catch {wm iconphoto . roster/user/unavailable}]} return

    trace variable ::curuserstatus w [namespace code update_icon]

    bind all <Map> +[namespace code {
	if {[string equal [winfo toplevel %W] %W]} { win_icon_setup %W }
    }]
}

##########################################################################

proc icon::win_icon_setup {w} {
    if {$w == "."} return

    switch -- [winfo class $w] {
	Chat {
	    wm iconphoto $w roster/conference/available
	}
	JDisco {
	    wm iconphoto $w roster/user/available
	}
	default {
	    wm iconphoto $w roster/user/available
	}
    }
}

##########################################################################

proc icon::update_icon {name1 {name2 ""} {op ""}} {
    global curuserstatus

    wm iconphoto . roster/user/$curuserstatus
}

##########################################################################

proc icon::update_all_icons {} {
    catch {
	foreach w [concat . [winfo children .]] {
	    win_icon_setup $w
	}
	update_icon curuserstatus
    }
}

hook::add set_theme_hook [namespace current]::icon::update_all_icons

##########################################################################

