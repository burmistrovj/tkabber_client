# $Id$

proc iq_browse_reply {xlib from child args} {
    set restags {}
    foreach ns [::xmpp::iq::registered $xlib] {
	lappend restags [::xmpp::xml::create ns -cdata $ns]
    }
    
    set res [::xmpp::xml::create query \
		    -xmlns jabber:iq:browse \
		    -attrs {category user \
			    type     client \
			    name     Tkabber} \
		    -subelements $restags]
    
    return [list result $res]
}

::xmpp::iq::register get * jabber:iq:browse \
		     [namespace current]::iq_browse_reply

