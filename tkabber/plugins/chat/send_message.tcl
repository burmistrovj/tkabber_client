# $Id$

proc send_message {chatid user body type} {
    set xlib [chat::get_xlib $chatid]
    set jid [chat::get_jid $chatid]
    set chatw [chat::chat_win $chatid]

    if {[hook::is_flag chat_send_message_hook send]} {
	set id [::xmpp::packetID $xlib]
	set command [list message::send_msg $xlib $jid \
			 -id $id \
			 -type $type \
			 -body $body]
	if {[info exists ::chat::chats(thread,$chatid)]} {
	    lappend command -thread $::chat::chats(thread,$chatid)
	}
	set xlist {}
	hook::run chat_send_message_xlist_hook xlist $chatid $user $id $body $type
	if {[llength $xlist] > 0} {
	    lappend command -xlist $xlist
	}
	lassign [eval $command] status x
	if {$status == "error"} {
	    return stop
	}

	set signP 0
	set encryptP 0
	foreach xe $x {
	    ::xmpp::xml::split $xe tag xmlns attrs cdata subels

	    switch -- $xmlns \
		$::NS(signed) {
		    set signP 1
		} \
		$::NS(encrypted) {
		    set encryptP 1
		}
	}

	if {![string equal $type groupchat]} {
	    if {$encryptP} {
		$chatw image create start_message -image gpg/encrypted
	    }
	    if {$signP} {
		$chatw image create start_message -image gpg/signed
	    }
	}
    }
    hook::unset_flag chat_send_message_hook send
}

hook::add chat_send_message_hook [namespace current]::send_message 90

