$Id$

HIGH

* Implement per-chat or per-JID enabling/disabling of
  message receipts. This presumably suggests implementing
  some general framework for "client-side privacy handling"
  on which this and other parts of Tkabber should be based
  (such as chat events and MUC ignoring).

* Provide for sending receipts along with normal messages.
  And displaying of "reception confirmed" status.
  It appears to require a patch to Tkabber.
  Also it probably will fit more naturally to the "mailbox"
  concept which will probably be eventually implemented for
  handling "normal" messages.


LOW

* Honor peer's features, if available.

* Think of implementing displaying of reception requests
  originating from chat peer: i.e. if peer reqtested a receipt
  for a given message, display an "empty" icon next to this
  message; if this request was satisfied, display "filled"
  icon, otherwise display "denied" icon.
  In fact this doesn't have much sense other than providing for
  better lineup of messages in a chat window which appears to
  be rather ragged now.

* Receipts should probably be sent when a message is displayed,
  not when it's received. This also fits more cleanly with
  displaying of remote reception requests.

* Try to decrease the size of icons (at least make them twice as
  this horizontally) -- may be they will look better.


# vim:tw=64:noet
