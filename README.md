#Tkabber client for internal usage

## OS: windows (winXP, win7, 8, 8.1) #

**Files:**

exec/win32bin - folder with "wish" executable and libraries for runing Tkabber in windows

exec/CP1251_keymap_ENtoRU.txt - a CP1251 encoding used to help with binding russian letters in tkabber

Tkabber - folder with current Tkabber version files

TKABBER_HOME - folder with configs for Tkabber to run properly, there are also "config.tcl" inside and "01.pem" key

-/plugins/ - folder with enabled plugins

-/config.tcl - main config file, changes in it affects all client program

-/01.pem - key for authentication to jabber.shipyard-yantar.ru Jabber Server (Still need AD profile)

tkabber_run.bat - used to run tkabber using bootstrap.tcl and using /TKABBER_HOME directory in root of Tkabber client package

tkabber_run(multiconnects).bat - used to run tkabber using bootstrap.tcl and using system defined %TKABBER_HOME% directory