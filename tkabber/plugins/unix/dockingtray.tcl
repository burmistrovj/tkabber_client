# $Id$

# KDE tray icon support.
# Requires Tk_Theme package
# (http://tkabber.jabber.ru/files/other/Tk_Theme-23.tgz)

##########################################################################

if {![cequal $::interface tk]} return

if {[catch { package require Tk_Theme }]} return

##########################################################################

namespace eval dockingtray {
    variable s2p
    foreach {k v} [list available   available      \
			away        away \
			chat        chat \
			dnd         dnd  \
			xa          xa   \
		        unavailable unavailable    \
		        invisible   invisible      \
		        blank       blank          \
			message1    message-server \
			message2    message        \
			message3    message-personal] {
        set s2p($k) docking/$v
    }

    variable options

    custom::defvar options(enable) 1 \
	[::msgcat::mc "Enable KDE tray icon."] \
	-group Systray -type boolean \
	-command [namespace code enable_disable]
}

##########################################################################

proc dockingtray::enable_disable {args} {
    variable options

    set icon .dockingtray

    if {$options(enable) && ![winfo exists $icon]} {
	ifacetk::systray::create $icon \
	    -createcommand [namespace code create] \
	    -configurecommand [namespace code configure] \
	    -destroycommand [namespace code destroy]
    } elseif {!$options(enable) && [winfo exists $icon]} {
	ifacetk::systray::destroy $icon
    }
}

hook::add finload_hook [namespace current]::dockingtray::enable_disable

##########################################################################

proc dockingtray::create {icon} {
    variable s2p

    set mb $icon.mb

    theme:frame $icon -kdesystray -class TkabberIcon

    label $mb -borderwidth 0 -image $s2p(unavailable) \
	      -highlightthickness 0 -padx 0 -pady 0
    pack $mb

    set m [ifacetk::systray::popupmenu $icon.menu]

    bind $mb <ButtonRelease-1> ifacetk::systray::restore
    bind $mb <<PasteSelection>> ifacetk::systray::withdraw
    bind $mb <<ContextMenu>> [list tk_popup [double% $m] %X %Y]
    balloon::setup $icon -command [list ifacetk::systray::balloon $icon]
}

##########################################################################

proc dockingtray::configure {icon status} {
    variable s2p

    if {![cequal $icon ""] && [winfo exists $icon]} {
	$icon.mb configure -image $s2p($status)
    }
}

##########################################################################

proc dockingtray::destroy {icon} {
    if {![cequal $icon ""] && [winfo exists $icon]} {
	::destroy $icon
    }
}

##########################################################################

