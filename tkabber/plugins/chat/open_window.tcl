# $Id$

proc chat_open_window {chatid from type body x} {
    if {$type eq "info"} {
	set type chat
    }
    chat::open_window $chatid $type -cleanroster 0
    set chatw [chat::chat_win $chatid]
    $chatw configure -state normal
}
hook::add draw_message_hook [namespace current]::chat_open_window 5

