#  namespaces.tcl --
#  
#      This file is part of the jabberlib. It lists Jabber
#      namespaces registered by JSF.
#      
#  Copyright (c) 2005 Sergei Golovan <sgolovan@nes.ru>
#   
# $Id$

##########################################################################

package provide namespaces 1.0

##########################################################################

namespace eval :: {
    array set NS [list \
	roster	    "jabber:iq:roster" \
	signed      "jabber:x:signed" \
	encrypted   "jabber:x:encrypted" \
	iqavatar    "jabber:iq:avatar" \
	xavatar     "jabber:x:avatar" \
	xconference "jabber:x:conference" \
	event       "jabber:x:event" \
	xroster     "jabber:x:roster" \
	rosterx     "http://jabber.org/protocol/rosterx" \
	chatstate   "http://jabber.org/protocol/chatstates" \
	commands    "http://jabber.org/protocol/commands" \
	private     "jabber:iq:private" \
	delimiter   "roster:delimiter" \
	bookmarks   "storage:bookmarks" \
	tkabber:groups "tkabber:bookmarks:groups" \
	pubsub      "http://jabber.org/protocol/pubsub" \
	nick        "http://jabber.org/protocol/nick" \
    ]
}

##########################################################################

