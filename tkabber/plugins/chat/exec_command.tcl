# $Id$

proc handle_exec_command {chatid user body type} {
    if {[string equal -length 6 $body "/exec "]} {
	set iw [chat::input_win $chatid]
	set command [crange $body 6 end]
	set res [catch {set output [eval exec $command]} errMsg]
	if {$res} {
	    set msg $errMsg
	} else {
	    set msg $output
	}
	after idle [list $iw insert end "\$ $command\n" {} $msg]
	return stop
    }
}
hook::add chat_send_message_hook [namespace current]::handle_exec_command 15

proc exec_command_comps {chatid compsvar wordstart line} {
    upvar 0 $compsvar comps
    
    if {!$wordstart} {
	lappend comps {/exec }
    }
}

hook::add generate_completions_hook [namespace current]::exec_command_comps
