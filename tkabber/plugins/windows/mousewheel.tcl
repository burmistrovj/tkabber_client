# $Id$

namespace eval mousewheel {
}

proc mousewheel::convert_to_button {modifier d x y} {

    switch -- $modifier {
	shift {
	    set scroll_up <<ScrollLeft>>
	    set scroll_down <<ScrollRight>>
	}
	default {
	    set scroll_up <<ScrollUp>>
	    set scroll_down <<ScrollDown>>
	}
    }
    if {$d < 0} {
	for {set i 0} {$i > $d} {incr i -120} {
	    event generate [winfo containing $x $y] $scroll_down
	}
    } else {
	for {set i 0} {$i < $d} {incr i 120} {
	    event generate [winfo containing $x $y] $scroll_up
	}
    }
}

bind Text <MouseWheel> " "
bind ListBox <MouseWheel> " "

bind Text <Shift-MouseWheel> " "
bind ListBox <Shift-MouseWheel> " "

bind all <MouseWheel> \
     [list [namespace current]::mousewheel::convert_to_button none %D %X %Y]
bind all <MouseWheel> +break
bind all <Shift-MouseWheel> \
     [list [namespace current]::mousewheel::convert_to_button shift %D %X %Y]
bind all <Shift-MouseWheel> +break

